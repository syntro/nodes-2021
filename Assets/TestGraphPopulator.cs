using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGraphPopulator : MonoBehaviour
{

    GraphManager graph;

    NodeData data1;
    NodeData data2;
    NodeData data3;

    [ExposeMethodInEditor]
    public void Add1()
    {
        graph.CreateNewNode(data1);
    }

    [ExposeMethodInEditor]
    void add2()
    {
        graph.CreateNewNode(data2);
    }


    [ExposeMethodInEditor]
    void add3()
    {
        graph.CreateNewNode(data3);
    }

    void Start()
    {
        Debug.Log("graph has test populator component active");
        graph = GetComponent<GraphManager>();
        if (graph == null) { Debug.Log("graph not found"); }


        data1 = new NodeData();
        data1.name = "qwe";
        data1.type = "task";
        //data.description = descriptionField.text;
        data1.GUID = data1.name + " " + System.DateTime.UtcNow.ToString();



        data2 = new NodeData();
        data2.name = "qweqwe";
        data2.type = "task";
        //data.description = descriptionField.text;
        data2.GUID = data2.name + " " + System.DateTime.UtcNow.ToString();


        data3 = new NodeData();
        data3.name = "qwer";
        data3.type = "task";
        //data.description = descriptionField.text;
        data3.GUID = data3.name + " " + System.DateTime.UtcNow.ToString();



        StartCoroutine(Populate());



    }

IEnumerator Populate()
    {
        yield return new WaitForSeconds(1);
        graph.CreateNewNode(data1);
        graph.CreateNewNode(data2);
        graph.CreateNewNode(data3);
    }
}