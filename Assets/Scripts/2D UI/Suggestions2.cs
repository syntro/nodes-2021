using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Suggestions2 : ListDisplay
{ // RENAME TO SuggestionsListDisplay

    // TODO   tags -> add to searchfield // if tag is chosen, suggestions are rerun with filtering by tag
    // set input field width to fit input text width


    Dictionary<string, NodeMono> NodesByName;

    public GraphManager graph;
    public InputField inputField;


    public override void SelectItem(MonoBehaviour selectedMono)
    { // this is called by ListElement.OnPointerClick()
        ClearList();
        inputField.text = selectedMono.name; // this depends on gameobject naming

        if (selectedMono is NewNodeForm)
        {
            Debug.Log("TODO:  show wizard in searchbar's position, hide searchbar");
            NewNodeForm wizard = (NewNodeForm)selectedMono;
            wizard.transform.position = this.transform.position;
            wizard.transform.rotation = this.transform.rotation;


        }
        else if (selectedMono is NodeMono)
        {
            Debug.Log(((NodeMono)selectedMono).data.GUID);
        }
        else if (selectedMono is EdgeMono)
        {
            Debug.Log("no action for EdgeMono");
        }
        else
        {
            Debug.Log("action for this type of attachment is not implemented");
        }

    }

    void PopulateSearchfield()
    {
        NodesByName.Clear();

        foreach (NodeMono node in graph.createdNodes)
        {
            if (!NodesByName.ContainsKey(node.data.name))
            {
                NodesByName.Add(node.data.name, node);
                node.PrintData("added to searchfield   ");
            }

        }

        Debug.Log("searchfield size == " + NodesByName.Count);


    }

    private void DisplayNewSuggestions(string queryString)
    {

        //List<string> matchedStrings = new List<string>();
        List<NodeMono> matchedNodes = new List<NodeMono>();

        foreach (string currentlyComparedString in NodesByName.Keys)
        {
            if (currentlyComparedString.Contains(queryString))
            {
                // matchedStrings.Add(currentlyComparedString);
                matchedNodes.Add(NodesByName[currentlyComparedString]);

            }
        }

        // first sorting: by distance on graph
        // this should go to inherited class
        // foreach item on tempList
        // ask graph to return distance between connecting Node      and Match
        // second sorting: move elements that begin with query to top

        //List<string> sortedSuggestions = new List<string>();

        //    listDisplay.DisplayStringList(matchedStrings);
        this.DisplayNodeList(matchedNodes);

        NodeMono exactMatch = null;
        foreach(NodeMono match in matchedNodes)
        {
            if(match.data.name == queryString)
            {
                exactMatch = match;
            }
        }
        if (exactMatch == null)
        {
            AddRowOnTop("New Node", graph.GetNodeWizard());
        }
        
    }

    private void OnEnable()
    {
        if (NodesByName != null) { PopulateSearchfield(); }
    }

    public void Start()
    {
        NodesByName = new Dictionary<string, NodeMono>();
        ListDisplay listDisplay = GetComponent<ListDisplay>();
        if (!listDisplay) { Debug.Log("listDisplay component not found in suggestions object"); }
        listDisplay.txtFile = null;


        inputField.onValueChanged.AddListener(DisplayNewSuggestions);

    }

}
