﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class NewNodeForm : MonoBehaviour
{
    public Button creationButton;

    public GraphManager dataManager;

    public InputField nameField;
    public InputField typeField;
    public InputField descriptionField;

    public List<InputField> fields;

    InputField lastFocusedField;

    public void CreateNodeFromForm()
    {
        NodeData data = new NodeData();
        data.name = nameField.text;
        data.type = typeField.text;
        data.description = descriptionField.text;
        data.GUID = data.name + " " + System.DateTime.UtcNow.ToString();

        // powyzsze zastap
        dataManager.CreateNewNode(data);

        Debug.Log("created node from data.name = " + data.name);

        ClearForm();


    }

    void ClearForm()
    {
        foreach(InputField field in fields) { field.text = ""; }

    }

    [ExposeMethodInEditor]
    public void JumpToNextField()
    {
        // to musi byc poinformowane z input fields 
        int lastFocusedIndex = fields.IndexOf(lastFocusedField);

        if (lastFocusedIndex < fields.Count - 1)
        {
            JumpToField(lastFocusedIndex + 1);
        }
        else
        {
            JumpToField(0);
        }


    }

    [ExposeMethodInEditor]
    public void JumpToPreviousField()
    {
        int lastFocusedIndex = fields.IndexOf(lastFocusedField);

        if (lastFocusedIndex > 0)
        {
            JumpToField(lastFocusedIndex - 1);
        }
        else
        {
            JumpToField(fields.Count-1);
        }
        
    }

    void JumpToField(int index)
    {
        fields[index].Select();
    }

    void Start()
    {
        fields = new List<InputField>();
        fields.Add(nameField);
        fields.Add(typeField);
        fields.Add(descriptionField);


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                JumpToPreviousField();
            }
            else
            {
                JumpToNextField();
            }

            //if (Input.GetKeyDown(KeyCode.Return))
            //{
            //    Debug.Log("return key pressed");
            //    if (Input.GetKey(KeyCode.LeftCommand)) // MAC ONLY FOR NOW
            //    {
            //        creationButton.onClick.Invoke();
            //    }
            //    else
            //    {
            //        if (lastFocusedField == descriptionField) // poki co to jest ostatnie pole
            //        {

            //        }
            //        else
            //        {
            //            JumpToNextField();
            //        }
            //    }

            //}


        }
        
       

        // to nie powinno chodzic tak w kółko. niech zmiana bedzie triggerowana z input fielda;
        foreach(InputField field in fields)
        {
            if (field.isFocused)
            {
                lastFocusedField = field;
            }
        }
    }
}
