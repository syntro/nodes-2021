using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ListElement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{


    public LayoutElement rowIndexLayoutElement;
    public MonoBehaviour attachedObject;

    public Text textObject; 
    public Text rowIndexText;

    public Color highlightedColor = Color.blue;
    public Color neutralColor;

    public void SetText(string textToDisplay)
    {
        textObject.text = textToDisplay;
    }

    public string GetText()
    {
        return textObject.text;
    }


    public void SetIndexPanelWidth(float newWidth)
    {
        rowIndexLayoutElement.minWidth = newWidth;
    }

   
    void Start()
    {
        NullChecks();

    }

    void NullChecks()
    {
        if (!textObject) { Debug.Log("ListElement: list element: text object not found"); }
        if (!rowIndexText) { Debug.Log("ListElement: rowIndexText component not found"); }
        if (!rowIndexLayoutElement) { Debug.Log("ListElement: rowIndexLayoutElement component not found"); }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GetComponent<Image>().color = highlightedColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = neutralColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ListDisplay listDisplay = GetComponentInParent<ListDisplay>();
        if (listDisplay == null) { Debug.Log("ListDisplay component not found in parent of this ListElement"); }
        listDisplay.SelectItem(this.attachedObject);
            
      
        
    }
}
