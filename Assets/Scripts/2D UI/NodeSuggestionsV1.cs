using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NodeSuggestionsV1 : MonoBehaviour, IPointerClickHandler
{

    // TODO   tags -> add to searchfield // if tag is chosen, suggestions are rerun with filtering by tag
    // set input field width to fit input text width


    Dictionary<string, NodeMono> NodesByName;

    public ListDisplay listDisplay;
    public GraphManager graph;
    public InputField inputField;


    //public void SelectItem(MonoBehaviour selectedMono)
    //{ // this is called by ListElement.OnPointerClick()
    //    Debug.Log("click detected on list element: " + selectedMono.name);
    //}

    void PopulateSearchfield()
    {
        NodesByName.Clear();

        foreach(NodeMono node in graph.createdNodes)
        {
            if (!NodesByName.ContainsKey(node.data.name))
            {
                NodesByName.Add(node.data.name, node);
                node.PrintData("added to searchfield   ");
            }

        }

        Debug.Log("searchfield size == " + NodesByName.Count);

        
    }
      
    private void DisplayNewSuggestions(string queryString)
    {
        //List<string> matchedStrings = new List<string>();
        List<NodeMono> matchedNodes = new List<NodeMono>();

        foreach (string currentlyComparedString in NodesByName.Keys)
        {
            if (currentlyComparedString.Contains(queryString))
            {
               // matchedStrings.Add(currentlyComparedString);
                matchedNodes.Add(NodesByName[currentlyComparedString]);
                
            }
        }

        // first sorting: by distance on graph
        // this should go to inherited class
        // foreach item on tempList
        // ask graph to return distance between connecting Node      and Match
        // second sorting: move elements that begin with query to top

        //List<string> sortedSuggestions = new List<string>();

        //    listDisplay.DisplayStringList(matchedStrings);
        listDisplay.DisplayNodeList(matchedNodes);  
    }

    private void OnEnable()
    {
        if (NodesByName != null) { PopulateSearchfield(); }
    }

    public void Start()
    {
        NodesByName = new Dictionary<string, NodeMono>();
        ListDisplay listDisplay = GetComponent<ListDisplay>();
        if (!listDisplay) { Debug.Log("listDisplay component not found in suggestions object"); }
        listDisplay.txtFile = null;


        inputField.onValueChanged.AddListener(DisplayNewSuggestions);
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        PopulateSearchfield();
    }
}
