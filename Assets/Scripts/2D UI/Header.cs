using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Header : MonoBehaviour
{
    public LayoutElement layoutElement;


    public GameObject iconImageGO;

    public Text textObject;
    private string text;
    public string Text
    {
        get { return textObject.text; }
        set { textObject.text = value; }
    }


    private void Start()
    {
        GetComponents();

    }

    public void GetComponents()
    {
        layoutElement = GetComponent<LayoutElement>();
        if (!layoutElement) { Debug.Log("header: layout element component not found"); }
        textObject = GetComponentInChildren<Text>();
        if (!textObject) { Debug.Log("header: layout element component not found"); }
    }

}
