using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EdgeDisplay : MonoBehaviour
{

    public Transform camera;
    MovementController3d movementController;
    

    public EdgeMono edgeMono;
    private Transform anchor;

    public Text labelText;
    public Text statusText;
    public Text commentText;
    public Text fooName;
    public Text barName;
    float scalingSpeed = 0.2f;

    private bool isHidden;
    public bool IsHidden
    {
        get { return isHidden; }
        set { isHidden = value; }
    }

    [ExposeMethodInEditor]
    public void ToggleHideUnhide()
    {
 
        if (IsHidden)
        {
            StartCoroutine(Rescale(1f, scalingSpeed));
            IsHidden = false;
        } else
        {
            StartCoroutine(Rescale(0f, scalingSpeed));
            IsHidden = true;
        }



    }

    IEnumerator Rescale(float targetScale, float time)
    {
        Debug.Log("rescaling to  " + targetScale.ToString());
        Vector3 startingScale = transform.localScale;
        Vector3 endingScale = new Vector3(targetScale, targetScale, targetScale);
        for (float sElapsed = 0f; sElapsed<=time; sElapsed+=Time.deltaTime)
        {
            // transform.localScale = Vector3.Lerp(startingScale, endingScale, msElapsed / time);
            transform.localScale = Vector3.Lerp(startingScale, endingScale, sElapsed/time);

            yield return null;
        }
        transform.localScale = endingScale;
    }



    [ExposeMethodInEditor]
    public void LookAtCamera()
    {
        movementController.PivotTowards(camera.position);
    }



    [ExposeMethodInEditor]
    public void UpdatePosition()
    {
        transform.position = edgeMono.mainBody.position;
        //StickToCable();   
    }

    [ExposeMethodInEditor]
    void AlignToCable()
    {
        transform.rotation = edgeMono.mainBody.rotation;
        transform.Rotate(0, 90, 0);
 
    }



    private void Start()
    {
        if (edgeMono == null) { edgeMono = GetComponentInParent<EdgeMono>(); }
        movementController = GetComponent<MovementController3d>();
        if (!movementController) { Debug.Log("movement controller component not found on edge display"); }
   
    }

    private void Update()
    {
        UpdatePosition();
    }
}
