using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ListDisplay : MonoBehaviour
{

    public MonoBehaviour AttachedObjectClickedByUser;

    public bool showRowNumbers = true;
    public bool ShowRowNumbers
    {
        get
        {
            return rows[0].rowIndexText.IsActive();
        }
        set
        {
            if (value == true)
            {
                foreach (ListElement row in rows)
                {
                    row.rowIndexText.gameObject.SetActive(value);
                }
            }

            if (value == false)
            {
                foreach (ListElement row in rows)
                {
                    row.rowIndexText.gameObject.SetActive(false);
                }
            }


        }
    }

    public virtual void SelectItem(MonoBehaviour selectedMono)
    { // this is called by ListElement.OnPointerClick()
        Debug.Log("click detected on list element: " + selectedMono.name);
    }


    [ExposeMethodInEditor]
    public void ShowRowNumberss()
    {
        ShowRowNumbers = true;
    }
    [ExposeMethodInEditor]
    public void HideRowNumberss()
    {
        ShowRowNumbers = false;
    }
 //       showRowNumbers = false;






        //SETTINGS

    public float maxHeight=500f;
    public bool isAdaptiveHeightOn = true;      // this should be a property

            

    public TextAsset txtFile;

    //OBJECTS
    public GameObject ElementTEMPLATEgo;
    public List<ListElement> rows;
    public Transform ContentHolder;         // should bot be public ?
    RectTransform ScrollViewRectTransform;  // should not be public ?
    public Header header;                   // should not be public ?




    /// <summary>
    /// DISPLAYING TEXT
    /// </summary>

    private string headerText;    // czy ten wiersz jest w ogóle potrzebny?
    public string HeaderText
    {
        get { return header.textObject.text; }
        set { header.textObject.text = value; }
    }


    public void DisplayNodeList(List<NodeMono> nodes)
    {
        ScrollViewRectTransform = GetComponentInChildren<ScrollRect>().GetComponent<RectTransform>();
        ClearList();
        foreach (NodeMono node in nodes)
        {
            
            AddRowOnBottom(node.data.name, node);
            Debug.Log("row added" + node.data.name);
        }

        AdaptIndexColumnWidth();

    }

    public void DisplayStringList(List<string> stringList)
    {
        ScrollViewRectTransform = GetComponentInChildren<ScrollRect>().GetComponent<RectTransform>();
        ClearList();
        foreach (string str in stringList)
        {
            AddRowOnBottom(str);
        }

        AdaptIndexColumnWidth();

    }

    void AdaptIndexColumnWidth()
    {
        
        if (rows.Count >0 && rows[0].rowIndexText.gameObject.activeSelf)
        {
            if (rows.Count > 99)
            {
                SetIndexColumnWidth(40f);
            } 
        }
    }


    public void DisplayString(string stringToDisplay)
    {
        List<string> lines = new List<string>();
        using (System.IO.StringReader reader = new System.IO.StringReader(stringToDisplay))
        {
            while (reader.Peek() > -1)
            {
                lines.Add(reader.ReadLine());
            }
        }
        DisplayStringList(lines);
    }

    [ExposeMethodInEditor]
    public void DisplayTextAsset()
    {
        
        header.iconImageGO.SetActive(true);
        header.Text = AssetDatabase.GetAssetPath(txtFile);
        //header.Text = "AssetDatabase is not availble outside unity editor";
        if (header.Text != null)
        {
            DisplayString(txtFile.text);
        }

    }

    public void SetIndexColumnWidth(float newWidth)
    {
        foreach (ListElement row in rows)
        {
            row.SetIndexPanelWidth(newWidth);
        }
    }



    /// <summary>
    /// ADDING AND REMOVING ELEMENTS
    /// </summary>

    public void AddRow(int newRowIndex, string text = "...", MonoBehaviour attachedObject = null)
    {
        if (newRowIndex > rows.Count)
        {
            newRowIndex = rows.Count;
        }
        GameObject newRowGO = GameObject.Instantiate(ElementTEMPLATEgo, ContentHolder);
        ListElement newElement = newRowGO.GetComponent<ListElement>();
        if (!newElement) { Debug.Log("list element not found)"); }
        rows.Insert(newRowIndex, newElement);
        newRowGO.transform.SetSiblingIndex(newRowIndex);

        newElement.SetText(text);

        newRowGO.name = "- ROW: " + text;
        newRowGO.SetActive(true);

        newElement.attachedObject = attachedObject;

        //
        if (isAdaptiveHeightOn)
        {
            AdjustHeightToContent();
        }

        UpdateDisplayedRowIndexes();

        //

    }

    public void RemoveRow(int index)
    {
        if (isAdaptiveHeightOn)
        {
            ListElement element = rows[index];
            rows.Remove(element);
            GameObject.Destroy(element.gameObject);
            AdjustHeightToContent();
        }
    }

    [ExposeMethodInEditor]
    public void ClearList()
    {
        SetIndexColumnWidth(20f);
        int count = rows.Count-1;
        for (int i = count; i > -1; i -= 1)
        {
            RemoveRow(i);
        }
    }


    public void AddRowOnTop(string text, MonoBehaviour attachedObject = null)
    {
        AddRow(0, text, attachedObject);
    }

    
    public void AddRowOnBottom(string text, MonoBehaviour attachedObject = null)
    {
        AddRow(rows.Count, text, attachedObject);
    }





    [ExposeMethodInEditor]
    public void SetIndexColumnWidth40()
    {
        SetIndexColumnWidth(40f);
    }


    /// <summary>
    /// DIMENSIONS
    /// </summary>
    private float headerHeight;   // czy ten wiersz jest w ogóle potrzebny?
    public float HeaderHeight
    {
        get
        {
            if (!header.layoutElement)
            {
                header.GetComponents();
            }
            return header.layoutElement.preferredHeight;
        }

        set { header.layoutElement.preferredHeight = value; }
    }

    [ExposeMethodInEditor]
    public void DoIHaveScrollRect()
    {
      
        Debug.Log(ScrollViewRectTransform);
    }

    private float bodyHeight; // czy ten wiersz jest w ogóle potrzebny?
    public float BodyHeight
    {
        get { return ScrollViewRectTransform.GetHeight() - HeaderHeight; }
        set {
            //Debug.Log(gameObject);
               ScrollViewRectTransform.SetSizeY(value); 
            //ScrollViewRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, value)
            }
    }

    public float testBodyHeight;
    [ExposeMethodInEditor]
    public void SetHeightTEST()
    {
        BodyHeight = testBodyHeight;
    }

    [ExposeMethodInEditor]
    public void SetHeight320()
    {
        BodyHeight = 320f;
    }

    private float bodyWidth; // czy ten wiersz jest w ogóle potrzebny?
    public float BodyWidth
    {
        get {
            LayoutElement body = ScrollViewRectTransform.GetComponent<LayoutElement>();
            if (!body) { Debug.Log("body LayoutElement not found"); }
            return body.preferredWidth; }

        set {
            LayoutElement body = ScrollViewRectTransform.GetComponent<LayoutElement>();
            if (!body) { Debug.Log("body LayoutElement not found"); }
            body.preferredWidth = value; }
    }


    //private float headerWidth;
    public float HeaderWidth
    {
        get { return header.layoutElement.preferredWidth; }
        set { header.layoutElement.preferredWidth = value; }
    }

    public void AdjustHeightToContent()
    {
        float newHeight = GetRowHeight() * rows.Count;
        if (!(newHeight > maxHeight))
        {
            BodyHeight = newHeight;
        }
        else
        {
            BodyHeight = maxHeight;
        }

    }


    public float GetRowHeight()
    {
        LayoutElement referenceRow;

        if (rows.Count > 0)
        { referenceRow = rows[0].GetComponent<LayoutElement>(); }
        else
        { referenceRow = ElementTEMPLATEgo.GetComponent<LayoutElement>(); }
        if (!referenceRow) { Debug.Log("ListDisplay.GetRowHeight: layout element not found"); }

        return referenceRow.preferredHeight;
    }


    /// <summary>
    /// VIEW SETTINGS
    /// </summary>
    ///

    void UpdateDisplayedRowIndexes()
    {
        if (rows[0].rowIndexText.gameObject.activeSelf)
        {
            for (int i = 0; i < rows.Count; i += 1)
            {
                rows[i].rowIndexText.text = i.ToString();
            }
        }
    }


    

    IEnumerator MinimizeBody(float targetWidth, float stepSize=40f)
    {
        Debug.Log("ChangeBodyWidth(" + targetWidth.ToString() + "f)  coroutine started");

        for (float width = BodyWidth; width > targetWidth; width -= stepSize) 
        {
            BodyWidth -= stepSize;
            yield return null;
        }

        for (float height = BodyHeight; height >=3f; height -= stepSize)
        {
            BodyHeight = height;
            Debug.Log(BodyHeight);

        }
    }

    [ExposeMethodInEditor]
    public void Minimize()
    {
        StartCoroutine(MinimizeBody(40f));

       //BodyWidth = 5f;

    }






    private void Start()
    {
        rows = new List<ListElement>();
        header = GetComponentInChildren<Header>();
        if (!header) { Debug.Log("ListDisplay: header component not found in children"); }

        
        ScrollViewRectTransform = GetComponentInChildren<ScrollRect>().GetComponent<RectTransform>();
        if (!ScrollViewRectTransform) { Debug.Log("ListDisplay rect transform not found"); }
        //else { Debug.Log(ScrollViewRectTransform.gameObject); }

        ElementTEMPLATEgo.SetActive(false);


        if (txtFile)
        {
            DisplayTextAsset();

        } 


        
    }





}
