using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{

    public List<NodeMono> nodesOnThisPlane = new List<NodeMono>();

    public Material transparentMaterial;
    public Material highlightedMaterial1;
    public Material highlightedMaterial2;

    public Vector3 OfferNodeGlobalPosition()
    {
        return transform.TransformPoint(new Vector3(-15 + (5* nodesOnThisPlane.Count),0,0));
    }

    public void AdoptNode(NodeMono node)
    {
        //todo: remove from last plane's node list

        node.transform.SetParent(this.transform);
        node.MoveTo(OfferNodeGlobalPosition());
        nodesOnThisPlane.Add(node);

    }

    public void Highlight(int mode = 1)
    {
        MeshRenderer renderer = GetComponentInChildren<MeshRenderer>();

        switch (mode)
        {
            case 0:
                renderer.material = transparentMaterial;
                break;
            case 1:
                renderer.material = highlightedMaterial1;
                break;
            case 2:
                renderer.material = highlightedMaterial2;
                break;
            default:
                renderer.material = highlightedMaterial1;
                break;

        }
    }

    private void Start()
    {
    
        // nodesOnThisPlane = new List<NodeMono>();
    }
}
