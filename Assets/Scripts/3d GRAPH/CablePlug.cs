using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CablePlug : MonoBehaviour, IClickable, IDraggable
{
    public Cable cable;

    public Transform socketThisIsAttachedTo;
    public Transform rememberedSocket;
    public Transform collidingSocket;

    public bool ignoreSocket = false;
    public Material idleMaterial;
    public Material clickabilityAdvertismentMaterial;
    bool isThisCurrentlyDragged;

    public void MakeConnection() //called on drop
    {
        Debug.Log(" CablePlug.MakeConnection() called");
        
        if (collidingSocket) // if dropped on socket - connect to it
        {
            socketThisIsAttachedTo = collidingSocket;
            Debug.Log("connection established "+ socketThisIsAttachedTo.gameObject.name  +"    // TODO: manifest this in data");

            
        }
        else // if dropped on plane - show Node Searchfield with option to create new node
        {
            Debug.Log("plug dropped on empty space. // TODO: type node name to find or create new and establish connection");
        }

        
    }


    public void AdvertiseInteractability()
    {
        //Debug.Log("plug under cursor");
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.material = clickabilityAdvertismentMaterial;
        }
    }

    public void StopAdvertising()
    {
        //Debug.Log("plug no longer under cursor");
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.material = idleMaterial;
        }
    }




    public void OnClick()
    {
        Debug.Log("plug clicked");
    }

    public void OnDragStart()
    {

        isThisCurrentlyDragged = true;
        rememberedSocket = socketThisIsAttachedTo;
        socketThisIsAttachedTo = null;

        Debug.Log("drag start.");
    }

    public void OnDragEnd()
    {
        Debug.Log("drag end");
        
        MakeConnection();
        isThisCurrentlyDragged = false;
    }

    public Transform GetDraggableTransform()
    {
        return transform;
    }

    public Plane GetPlaneThisIsOn()
    {
        return GetComponentInParent<Plane>();
    }

    public void RememberPlane(Plane plane)
    {
        lastPlaneTouched = plane;
        Debug.Log(gameObject.name + " remembering: " + plane.gameObject.name);
    }

    // DETECT CONNECTABLE

    void OnTriggerEnter(Collider other)
    {
        
        if (socketThisIsAttachedTo == null)
        {
            NodeSocket collidingNodeSocket = other.GetComponentInParent<NodeSocket>(); // zamień na IConnectable
        
            if (collidingNodeSocket)
            {
                //Debug.Log("colliding with " + collidingNodeSocket.gameObject.name);
                collidingSocket = collidingNodeSocket.GetSocketTransform();
                Debug.Log("colliding with " + collidingSocket.gameObject.name);

                // Remembering socket for connection on drag end

                collidingNodeSocket.AdvertiseInteractability();
                this.AdvertiseInteractability();
            }




        }
    }

    void OnTriggerExit(Collider other)
    {
        if (isThisCurrentlyDragged)
        {
            if (socketThisIsAttachedTo != null)
            {
                socketThisIsAttachedTo = null;                                      // releasing socket 
                Debug.Log("socket disconnected");
            }

            NodeSocket collidingNodeSocket = other.GetComponentInParent<NodeSocket>(); // zamień na IConnectable
            if (collidingNodeSocket)
            {
                collidingNodeSocket.StopAdvertising();
                this.StopAdvertising();

                collidingSocket = null;                                                // forgetting socket
            }
        }

    }



    // refactor this to CablePlug

    // draggable
    // dragScrollable



    //public void HighlightThisEnd()
    //{
    //    Renderer renderer = GetComponent<Renderer>();
    //    if (!renderer) { Debug.Log("renderer not found on CablePlug"); }


    //    Debug.Log("highlight called");
    //    //renderer.material = 
    //}


    //public void OnMouseOver()
    //{
    //    HighlightThisEnd();
    //}

    [ExposeMethodInEditor]
    public void UpdatePosition()
    {

        if (!socketThisIsAttachedTo)
        {    // trzymaj stałą pozycje wzgledem cable.mainBody.position
            return;
        }
       
        if (!ignoreSocket)
        {transform.position = socketThisIsAttachedTo.position; }
    }

    [ExposeMethodInEditor]
    public void UpdateRotation()
    {
        
        transform.rotation = cable.mainBody.rotation;
    }



    private void Update()
    {
        UpdatePosition();
    }


    private void Start()
    {
        cable = transform.parent.GetComponentInChildren<Cable>();
        if (!cable) { Debug.Log("Cable component not found in sibilings"); }

    }

    Plane lastPlaneTouched;

    public Plane GetLastPlane()
    {
        return lastPlaneTouched;
    }

    public Transform OnHoldDetected()
    {
        Debug.Log(gameObject.name + " pointer hold detected");
        return transform;
    }
}
