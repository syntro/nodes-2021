﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGizmo : MonoBehaviour//, IClickabl
{
    public Material idleNodeMaterial;
    public Material clickabilityNodeMaterial;


    public Transform GizmoBody;
    public Transform graphSocket;
    
public void AdvertiseInteractability()
    {
        GizmoBody.GetComponent<Renderer>().material = clickabilityNodeMaterial;
        
    }


    public void StopAdvertising()
    {
        GizmoBody.GetComponentInChildren<MeshRenderer>().material = idleNodeMaterial;
    }


//    public void OnClick()
//    {
//        throw new System.NotImplementedException();
//    }




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Transform IDraggable.GetDraggableTransform()
    //{
    //    return GetComponentInParent<NodeMono>().transform;
    //}

    //Plane IDraggable.GetPlaneThisIsOn()
    //{
    //    return GetComponentInParent<Plane>();
    //}

    //void IDraggable.OnDragStart()
    //{
    //    //Debug.Log("node drag start");
    //}

    //void IDraggable.OnDragEnd()
    //{
    //    //Debug.Log("node drag end");
    //}
}
