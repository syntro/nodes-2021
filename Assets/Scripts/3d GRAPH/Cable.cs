using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cable : MonoBehaviour, IClickable
{
    public Transform mainBody;
    Transform mainCylinder;
    public CablePlug[] plugs = new CablePlug[2];
    //public Transform[] internalplugs = new Transform[2];
    private Vector3[] lastPlugsPositions = new Vector3[2];


    public Material idleMaterial;
    public Material clickabilityAdvertismentMaterial;

    public void AdvertiseInteractability()
    {
        mainBody.GetComponentInChildren<MeshRenderer>().material = clickabilityAdvertismentMaterial;
    }

    public void StopAdvertising()
    {
        mainBody.GetComponentInChildren<MeshRenderer>().material = idleMaterial;
    }

    virtual public void OnClick()
    {
        Debug.Log("cable clicked");
    }

    public Transform OnHoldDetected()
    {
        Debug.Log(gameObject.name + " pointer hold detected");
        return transform;
    }

    public void ToggleMinimize()
    {
        Debug.Log("toggle minimize called");
    }

    [ExposeMethodInEditor]
    public void ReorientCable()
    {

        UpdatePlugPositions();                                                  //teleporting plugs to socket positions

        if (plugs[0].transform.position == lastPlugsPositions[0])               //checking if plug positions have changed
        {
            if (plugs[1].transform.position == lastPlugsPositions[1])
            {
                return;
            }
        }
        
        lastPlugsPositions[0] = plugs[0].transform.position;                     //remembering current plug positions
        lastPlugsPositions[1] = plugs[1].transform.position;

        
        
        mainBody.position = Vector3.Lerp(plugs[0].transform.position, plugs[1].transform.position, 0.5f);   //positioning body between plugs

        mainBody.transform.LookAt(plugs[1].transform);                          //rotating body between plug 0 and 1

        UpdatePlugRotations();

        float x = mainBody.localScale.x;                                                                    // resizing mainBody.localScale.z
        float y = mainBody.localScale.y;
        float z = Vector3.Distance(plugs[0].transform.position, plugs[1].transform.position) / 2;      
        mainBody.localScale = new Vector3(x, y, z);



    }

    private void LateUpdate()
    {
        ReorientCable();
  
    }


    private void Start()
    {
        mainCylinder = mainBody.GetComponentInChildren<CapsuleCollider>().transform;
        if (!mainCylinder) { Debug.Log("cylinder not found in cable"); }

        plugs = GetComponentsInChildren<CablePlug>();
        if (plugs.Length != 2) { Debug.Log("amount of cable plugs in children should be 2 and is " + plugs.Length.ToString() + " : " + plugs.ToString()); }


        lastPlugsPositions[0] = plugs[0].transform.position;
        lastPlugsPositions[1] = plugs[1].transform.position;
    }


    // HELPER METHODS / ENCAPSULATED CODE

    public void UpdatePlugPositions()
    {
        foreach (CablePlug plug in plugs)                                       // rotating plugs to align with body
        {
            plug.UpdatePosition();
        }
    }

    public void UpdatePlugRotations()
    {
        foreach (CablePlug plug in plugs)                                       // rotating plugs to align with body
        {
            plug.UpdateRotation();
        }
    }


}
