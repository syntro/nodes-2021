using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeSocket : MonoBehaviour, IClickable //, IDraggable // IConnectable
{
    public Material idleMaterial;
    public Material clickabilityAdvertismentMaterial;
    public Material SelectedMaterial;
    public Transform TransformTouchingEdge;

    public Transform GetSocketTransform()
    {
        return TransformTouchingEdge;
    }


    //public Transform GetDraggableTransform()
    //{
    //    return GetComponentInParent<NodeMono>().transform;
    //}

    //public Plane GetPlaneThisIsOn()
    //{
    //    return GetComponentInParent<Plane>();
    //}

    //public void OnDragStart()
    //{
    //    //Debug.Log("node drag start");
    //}

    //public void OnDragEnd()
    //{
    //    //Debug.Log("node drag end");
    //}

    public void AdvertiseInteractability()
    {
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.material = clickabilityAdvertismentMaterial;
        }
    }

    public void StopAdvertising()
    {
       // Debug.Log("stop advertising");
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.material = idleMaterial;
        }
    }

    public void OnClick()
    {
       Debug.Log("socket click");
    }

    public Transform OnHoldDetected()
    {
        Debug.Log(gameObject.name + " pointer hold detected");
        // get graph manager
        GraphManager graph = GetComponentInParent<GraphManager>();
        if (!graph)
        {
            Transform parent = transform.parent;
            while ( parent != null)
            {
                parent = parent.parent;
            }
            graph = parent.GetComponentInChildren<GraphManager>();
        }

        EdgeMono edge = graph.CreateEmptyEdge(TransformTouchingEdge);
        
        Debug.Log("new edge: " + edge.gameObject.name + "   should apear ready for dragging");

        return edge.plugs[1].transform;
        
    }

    private void Start()
    {
        if (GetComponent<Collider>())
        {
            Debug.Log("Node Drag Handle has a collider attached. Only colliders in children will be valid for clicking and dragging purposes");
        }   
    }
}
