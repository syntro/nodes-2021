﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeMono : MonoBehaviour, IClickable, IDraggable
{

    public NodeData data;

    public Text nameLabel;
    public Text typeLabel;
    public Text descriptionLabel;


    public Material idleMaterial;
    public Material clickabilityAdvertismentMaterial;
    public Material SelectedMaterial;

    public Transform cubeTransform;
    public Renderer bodyRenderer;
    public NodeGizmo gizmo;

    public Vector3 scaleMinimized = new Vector3(0.25f, 0.25f, 0.25f);
    public float defaultMoveSpeed = 100f;

    Plane lastPlaneTouched;


    List<NodeData> GetDirectlyConnectedNodes()
    {
        List<NodeData> nodes = new List<NodeData>();
        
        return nodes;
    }

    int EvaluateDistanceToNode(NodeMono target)
    {
        int distance = -1;

        //    foreach()

        return distance;
    }

    //public List<List<NodeMono>> PathsToNode(NodeMono target)
    //{
    //    List<List<NodeMono>> paths = new List<List<NodeMono>>()
    //    //
    //    if(paths[0][0] == null)
    //    {
    //        return null;
    //    } else
    //    {
    //        return paths;
    //    }

    //}

    public Plane GetLastPlane()
    {
        return lastPlaneTouched;
    }

    public void RememberPlane(Plane plane)
    {
        lastPlaneTouched = plane;
    }


    [ExposeMethodInEditor]
    public void PrintData(string prefix = "")
    {
        Debug.Log(prefix + "(" + data.type + ") " + data.name + " " + data.description);

    }

    [ExposeMethodInEditor]
    public void UpdateLabels()
    {
        nameLabel.text = data.name;
        typeLabel.text = data.type;
    //    descriptionLabel.text = data.description;
        this.gameObject.name = "NODE: " + data.name;
    }

    [ExposeMethodInEditor]
    public void Minimize()
    {
        //to zrób płynnie korutyną
        //cubeTransform.localScale  = new Vector3(scaleMinimized, scaleMinimized, scaleMinimized);
        cubeTransform.localScale = scaleMinimized;

        // wyłąd obiekt INDENTED PANEL
    }

    [ExposeMethodInEditor]
    public void Normalize()
    {
        //to zrób płynnie korutyną
        //cubeTransform.localScale  = new Vector3(scaleMinimized, scaleMinimized, scaleMinimized);
        cubeTransform.localScale = Vector3.one;


    }

    public void MoveTo(Vector3 targetGlobalPosition, float speed = 1f)
    {
        //MovementController3d movementController;
        //movementController.MoveToPosition(targetGlobalPosition);
    }


    public void AdvertiseInteractability()
    {
        gizmo.AdvertiseInteractability();
        //Debug.Log("cursor over child of" + gameObject.name);
    }

    public void StopAdvertising()
    {
        gizmo.StopAdvertising();
        //Debug.Log("cursor exit object");
    }

    public void OnClick()
    {
        Debug.Log("node click");
    }

    public Transform OnHoldDetected()
    {
        Debug.Log(gameObject.name + " pointer hold detected");
        return transform;
    }

    public Transform GetDraggableTransform()
    {
        return transform;
    }

    public Plane GetPlaneThisIsOn()
    {
        return GetComponentInParent<Plane>();
    }

    void IDraggable.OnDragStart()
    {
        //Debug.Log("node drag start");
    }

    void IDraggable.OnDragEnd()
    {
        //Debug.Log("node drag end");
    }
    private void Start()
    {
        gizmo = GetComponentInChildren<NodeGizmo>();
        if (!gizmo) { Debug.Log(gameObject.name + ": no gizmo found in NodeMono children"); }
    }



    //public Plane FindNearestPlane()
    //{

    //    return null;

    //}
    //IEnumerator ExpandSphereUntilItCollidesWithPlane()
    //{

    //}
}
