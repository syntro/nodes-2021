using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class EdgeData 
{
    public string[] guidPair = new string[2];
    public string relationshipType;
    public string comment;
    public List<string> tags;  // = new List<string>();
    
}
