using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NodeData
{
    public string GUID;
    public string name;
    public string type;
    public List<string> tags;
    public string description;
    public List<EdgeData> edges;
}
