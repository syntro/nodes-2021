using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDraggable
{
    
    public Transform GetDraggableTransform();
    public Plane GetPlaneThisIsOn();

    void OnDragStart();
    void OnDragEnd();
    Plane GetLastPlane();
    void RememberPlane(Plane plane);
   // Plane FindNearestPlane();


}
