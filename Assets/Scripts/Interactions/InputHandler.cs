﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    float singleClickTimeLimit = 0.2f;
    float pointerDownTimer;
    int pointerHoldFrameCounter;   // change this to bool? only need to know if there was dragging in previous frame

    LayerMask permablePlanesMask;
    LayerMask imPermablePlanesMask;
    LayerMask interactableObjectsMask = 0;

    PlaneSet lastUsedPlaneset;
    Plane currentDragigngPlane;

    public Camera currentCamera;
    Ray pointerRay;

    public Transform currentlyDraggedTransform;
    MovementController3d movementControllerOfDraggable;

    Vector3 cursorOffset;

    IClickable savedClickable;
    public IDraggable savedDraggable;

    void PointerUpdate()
    {
        pointerRay = currentCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit interactableHitInfo;
        IClickable hitClickable = null;

        if (Physics.Raycast(pointerRay, out interactableHitInfo, 1000f, interactableObjectsMask)) // raycast ignoring plane layers
        {
            
            hitClickable = interactableHitInfo.transform.GetComponentInParent<IClickable>();        // get clickable
               
            if (hitClickable != savedClickable)
            {
                
                if (savedClickable != null)
                {
                    
                    if (currentlyDraggedTransform == null)                                          // if not dragging
                    {
                        savedClickable.StopAdvertising();                                           // unhighlight and forget
                        savedClickable = null;
                    }
                }
            }

            if (hitClickable != null)       
            {
                hitClickable.AdvertiseInteractability();                                   // highlight
                savedClickable = hitClickable;                                             // remmember
            }
        }

        if (hitClickable == null)                                                           // if nothing under cursor
        {
            if (savedClickable != null)                                                     // if clickable is remembered
            {
                savedClickable.StopAdvertising();                                           // unhighlight and forget
                savedClickable = null;
            }
            
        }





        if (currentlyDraggedTransform != null)
        {
            DragUpdate();                                                                       // DRAG
        }
        else
        {

            if (Input.GetMouseButtonDown(0))                                                    // RESTART pointerDownTimer
            {
                pointerDownTimer = 0f;
            }

            if (Input.GetMouseButton(0))                                                        // INCREMENT pointerDownTimer
            {
                pointerDownTimer += Time.deltaTime;
            }

            if (Input.GetMouseButtonUp(0))
            {

                if (savedClickable != null)
                {
                    if (pointerDownTimer <= singleClickTimeLimit)
                    {
                        savedClickable.OnClick();                                                 // click action
                    }

                }

                if (currentlyDraggedTransform != null)
                {
                    savedDraggable.OnDragEnd();                                                 // drag_end action
                    savedDraggable = null;
                    currentlyDraggedTransform = null;
                    lastUsedPlaneset.DraggingDepth = 0;

                    // ray from currentlydragged away from camera
                    // if ray hits connectable
                    // conntect to it
                    // else
                    // display three options
                    // option 1: input box w. suggestions
                    // option 2: destroy edge button -> if done, let undo for 5 sec.
                    // option 3: reconnect to {remembered node name};
                }

                pointerDownTimer = 0f;
                pointerHoldFrameCounter = 0;


            }

            if (pointerDownTimer > singleClickTimeLimit)                                        //DETECT pointer hold
            {
                if (interactableHitInfo.transform != null)
                {
                    if (pointerHoldFrameCounter == 0)
                    {
                        Transform holdResult = savedClickable.OnHoldDetected();
                        if (holdResult)
                        {
                            currentlyDraggedTransform = holdResult;
                        }
                        
                    }
              

                    IDraggable hitDraggable = interactableHitInfo.transform.GetComponentInParent<IDraggable>(); //get hit draggable
                    if (hitDraggable == null) { Debug.Log("no draggable was hit"); }
                    if (hitDraggable != null)
                    {
                        savedDraggable = hitDraggable;                                      // remember hit draggable
                    }

                    if (currentlyDraggedTransform == null)                                      // GET DRAGGABLE TRANFSORM
                    {
                        if (savedDraggable != null)
                        {
                            currentlyDraggedTransform = savedDraggable.GetDraggableTransform();
                        }
                    }

                }

            }

        }

        void DragUpdate()
        {
            
            

            if (pointerHoldFrameCounter == 0)                                             
            {
                savedDraggable.OnDragStart();                                       // tell draggable that dragging begun

                currentDragigngPlane = savedDraggable.GetLastPlane();
                if (currentDragigngPlane)
                {
                    PlaneSet currentPlaneSet = currentDragigngPlane.GetComponentInParent<PlaneSet>();
                    currentPlaneSet.DraggingDepth = currentPlaneSet.planes.IndexOf(currentDragigngPlane);
                }


                cursorOffset = currentlyDraggedTransform.position - interactableHitInfo.point;  // CALCULATE CURSOR OFFSET between hit and dragged transform

            }
            pointerHoldFrameCounter += 1;                                                          // INCREMENT pointerHoldFrameCounter

            RaycastHit impermablePlaneHitInfo;
            if (Physics.Raycast(pointerRay, out impermablePlaneHitInfo, 1000f, imPermablePlanesMask)) // raycast for impermable planes
            {
                Vector3 destination = impermablePlaneHitInfo.point + cursorOffset;          // set destination

                currentlyDraggedTransform.position = destination;                           // REPOSITION
               
                savedDraggable.RememberPlane(impermablePlaneHitInfo.transform.GetComponentInParent<Plane>()); // remember Plane;

                if (Input.GetAxis("Mouse ScrollWheel") > 0f)
                {
                    PlaneSet currentPlaneSet = savedDraggable.GetLastPlane().GetComponentInParent<PlaneSet>();
                    currentPlaneSet.DecreaseDraggingDepth();
                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0f)
                {
                    PlaneSet currentPlaneSet = savedDraggable.GetLastPlane().GetComponentInParent<PlaneSet>();
                    currentPlaneSet.IncreaseDraggingDepth();
                }


                // hitinfo - get planeset
                // hitinfo - get plane index
                // set dragging depth Property to match index

                // on drag end reset dragging depth Property (at drag end action)

            }

   





            if (Input.GetMouseButtonUp(0))
            {
                savedDraggable.OnDragEnd();                                                 // drag_end sequence
                savedDraggable = null;
                currentlyDraggedTransform = null;
                pointerDownTimer = 0f;
                pointerHoldFrameCounter = 0;
            }
        }
    }
        


    

    


    private void Update()
    {
        PointerUpdate();
    }

    void Start()
    {

        permablePlanesMask = 1 << 8;
        imPermablePlanesMask = 1 << 9;
        interactableObjectsMask = ~(permablePlanesMask | imPermablePlanesMask);




        if (currentCamera == null)
        {
            currentCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
            if (currentCamera == null) { Debug.Log("NOT SET: Camera for InputHandler"); }
        }
    }


}
