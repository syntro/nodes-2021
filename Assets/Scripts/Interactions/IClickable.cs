﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClickable
{
    void AdvertiseInteractability();
    void StopAdvertising();
    void OnClick();
    Transform OnHoldDetected();
}

