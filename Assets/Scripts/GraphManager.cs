using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphManager : MonoBehaviour
{
    // dictionary <string, transform> guidToNodes
    public GameObject nodeTemplate;
    public GameObject edgeTemplate;
    public List<NodeMono> createdNodes = new List<NodeMono>();
    public Plane newNodePlane;

    public ListDisplay createdNodesListDisplay;

    public NewNodeForm GetNodeWizard()
    {

      NewNodeForm wizard = GetComponentInChildren<NewNodeForm>();
      if (wizard== null) { Debug.Log("new node form not found in graph's children"); }
      return wizard;
    }

    public void CreateNewNode(NodeData data)
    {
        NodeMono newNode = CreateEmptyNode();
        PopulateNode(newNode, data);
        createdNodesListDisplay.AddRowOnBottom(data.name, newNode);
    }


    public EdgeMono CreateEmptyEdge(Transform originTransform)
    {
        GameObject newGO = Instantiate(edgeTemplate, this.transform);
        EdgeMono edgeMono = newGO.GetComponent<EdgeMono>();

        edgeMono.plugs[0].socketThisIsAttachedTo = originTransform;
        edgeMono.plugs[1].socketThisIsAttachedTo = null;

        edgeMono.plugs[1].transform.position = originTransform.position;
        // offset position for cursor
        
        // drag socket 1 implemented between input handler and nodesocket
        return edgeMono;
    }


    public NodeMono CreateEmptyNode()
    {

        GameObject newGO = Instantiate(nodeTemplate, newNodePlane.transform);

        // move this to Plane.Adopt()
        newGO.transform.position = newNodePlane.OfferNodeGlobalPosition();
        newGO.transform.localRotation = Quaternion.identity;

        NodeMono node = newGO.GetComponent<NodeMono>();
        if (node == null) { Debug.Log("NodeMono was not found after instantiation"); }

        newNodePlane.nodesOnThisPlane.Add(node);
        createdNodes.Add(node);

        return node;
    }

    public void PopulateNode(NodeMono node, NodeData data)
    {
        node.data = data;
        node.UpdateLabels();
        node.PrintData("created ");

       // Debug.Log("populate node was called");
    }

    public void DeleteNode(NodeMono node)
    {
        int rowIndex = createdNodes.IndexOf(node);
        createdNodesListDisplay.RemoveRow(rowIndex);
        Destroy(node.gameObject);
        DeleteNode(node);

//        Debug.Log("delete node was called ");
    }


    public void DeleteAllNodes()
    {
        Debug.Log("DELETE ALL NODES was called");


        for (int countdown= createdNodes.Count; countdown>0; countdown -= 1)
        {
            DeleteNode(createdNodes[countdown]);
        }
        //foreach (NodeMono n in createdNodes)
        //{
        //    DeleteNode(n);
        //    //Destroy(n.gameObject);
        //    //DeleteNode(n);
        //}


    }



    // void CreateConnector(){}


    void Start()
    {
       if (nodeTemplate == null) { Debug.Log("Graph Manager: nodeTemplate is missing"); }
        if (edgeTemplate == null) { Debug.Log("Graph Manager: edgeTemplate is missing"); }
       if (createdNodesListDisplay == null) { Debug.Log("GrpahDataManager: createdNodesListDisplay not found"); }
       createdNodesListDisplay.header.Text = "EXISTING NODES";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
