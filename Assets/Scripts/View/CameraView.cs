using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraView : MonoBehaviour
{

    public Transform vantagePointTransform;
    public Transform viewGazePointer;

    public MyCameraController cam;


    [ExposeMethodInEditor]
    public void TeleportCameraNow()
    {
        cam.transform.position = transform.TransformPoint(Vector3.zero);
        transform.LookAt(viewGazePointer);
        cam.transform.rotation = transform.rotation;

    }

    //public Transform gizmo;

    [ExposeMethodInEditor]
    public void UpdateGizmoRotation()
    {
        transform.LookAt(viewGazePointer);
    }

    [ExposeMethodInEditor]
    public void Apply()
    {
        cam.ApplyView(this);
    }


    

    private void Start()
    {    

        UpdateGizmoRotation();
        
    }


}
