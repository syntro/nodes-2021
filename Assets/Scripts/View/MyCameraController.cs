using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCameraController : MonoBehaviour
{

    public MovementController3d movementController; // moze this powinno dziedziczyc po MovementController?
    public CameraView lastUsedView; 
    public MovementController3d gazePointerMovement;

    public List<CameraView> previousViews;


    public float speed = 100f;


    public void ApplyView(CameraView viewToApply)
    {

        gazePointerMovement.MoveTo(viewToApply.viewGazePointer);
        movementController.MoveTo(viewToApply.transform);


        //save and update
        if (!lastUsedView) { previousViews.Add(lastUsedView); }
        lastUsedView = viewToApply;

    }

    void LookAt(Transform pointerTF)
    {
        transform.LookAt(pointerTF, Vector3.up);
    }


    void Start()
    {
        previousViews = new List<CameraView>();
    }

    // Update is called once per frame
    void Update()
    {
        LookAt(gazePointerMovement.transform);

    }
}
