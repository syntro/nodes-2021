using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController3d : MonoBehaviour
{
    public bool keepLookingAtPointer;
    Transform container;
    public Transform savedRotationPointer;
    public Transform savedDestination;
  
    public float defaultMovementSpeed = 0.1f;
    public float defaultRotationSpeed = 0.1f;


    private void Update()
    {
        if (keepLookingAtPointer)
        {
            LookAt(savedRotationPointer);
        }
    }

    [ExposeMethodInEditor]
    public void MoveToDestination()
    {
        MoveTo(savedDestination);
    }


    [ExposeMethodInEditor]
    public void AlignToContainer()
    {
        Debug.Log("MovementController.AlighToContainer() called");
    }


    // PIVOTING

    public void PivotTowards(Vector3 pointerV3, float speed = 40f)
    {
        Vector3 leveledPointer = new Vector3(pointerV3.x, 0, pointerV3.z);

        StartCoroutine(RotateFacing(leveledPointer, defaultRotationSpeed));

        //horizontal-only rotation facing pointer
        Debug.Log("MovementController.PivotTowards() called");
    }

    IEnumerator RotateFacing(Vector3 targetV3, float speed)
    {
        Vector3 cameraUp = Vector3.up; // this should adapt to camera rotation
        Quaternion startRotation = transform.rotation;
        Quaternion endRotation = Quaternion.LookRotation(targetV3, cameraUp);
        float stepSize = 0.01f;
        for (float zeroToOne = 0; zeroToOne <= 1; zeroToOne += stepSize)
        {
            transform.rotation = Quaternion.Lerp(startRotation, endRotation, zeroToOne);
            yield return null;
        }
    }



    // KEEP LOOKING AT

    public void LookAt(Transform pointer)
    {
            transform.LookAt(savedRotationPointer, Vector3.up);
 
        // StartCoroutine(RotateFacing(pointer, defaultRotationSpeed));
    }

    // MOVING

    public void MoveToPosition(Vector3 destinationV3)
    {
        GameObject pointer = new GameObject("movement pointer");
        pointer.transform.position = destinationV3;
        MoveTo(pointer.transform);
    }


    public void MoveTo(Transform objective)
    {
       // Debug.Log("Movement.Controller.MoveTo() called for [ " + objective.position.ToString() + " ]");
        StartCoroutine(MoveTowards(objective, defaultMovementSpeed));
    }

    IEnumerator MoveTowards(Transform destinationTF, float speed)
    {
        Vector3 start = this.transform.position;
        float stepSize = speed / Vector3.Distance(start, destinationTF.position);

      //  Debug.Log("distance = " + Vector3.Distance(start, destinationTF.position).ToString());
      //  Debug.Log("step size = " + stepSize.ToString());

        for (float zeroToOne = 0; zeroToOne <= 1; zeroToOne += stepSize)
        {
            transform.position = Vector3.Lerp(start, destinationTF.position, zeroToOne);
            yield return null;
        }

        transform.position = destinationTF.position;


    }

    // 
    [ExposeMethodInEditor]
    public void Level()
    {
        Vector3 v3 = transform.localEulerAngles;
        transform.eulerAngles = new Vector3(0, v3.y, v3.z);

    }


    //[ExposeMethodInEditor]
    //public void LookAndPrint()
    //{
    //    transform.LookAt(savedRotationPointer, Vector3.up);
    //    PrintRotation();
    //}


    //[ExposeMethodInEditor]
    //public void PrintRotation()
    //{
    //    Debug.Log(transform.rotation.ToString());
    //}

    //IEnumerator RotateFacing(Transform pointer, float speed)
    //{

    //    Debug.Log("korutyna RotateFacing jeszcze nie działa");
    //    Quaternion start = transform.rotation;
    //    Quaternion end = Quaternion.LookRotation(pointer.position-transform.position);

    //    Debug.Log("start = " + start.ToString());
    //    Debug.Log("end = ) " + end.ToString());
    //    Debug.Log("start * quaternion.inverse(end) = " + (start * Quaternion.Inverse(end)).ToString());

    //    float stepSize = 0.01f;

    //    for (float zeroToOne = 0; zeroToOne <= 1; zeroToOne += stepSize)
    //    {
    //        transform.rotation = Quaternion.Lerp(start, end, stepSize);
    //        yield return null;





    //    }
    //    transform.rotation = end;
    //}


    private void Start()
    {
        if (savedDestination == null) { savedDestination = this.transform; }
        if(savedRotationPointer== null) { savedRotationPointer = this.transform; }
    }

}