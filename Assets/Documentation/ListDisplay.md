________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________
# ListDisplay (class)
    Displays a multiline string or a list of strings  using children gameObjects with ListElement components.

_________________________________________________
###    hierarchy dependencies

        this
            is attached to UI Panel
            panel has vertical layout group                        //  (only control child witdh checked, rest unchecked)

        children
            header panel
                Header Text
            body pannel with scroll view
                (#template-LIST-ELEMENT)                          //  always inactive

// TODO: resizing by dragging and dropping borders, corner  
// TODO: minimizing and reverting size
_________________________________________________

## **Public Variables**
_________________________________________________


    string HeaderText            -     property
    GameObject ElementTEMPLATEgo -     will be initialized in AddRow()
    TextAset TxtFile             -     (optional) 


    float maxHeight=500f;
    bool isAdaptiveHeightOn = true;   // TODO: this should be made a property

_________________________________________________
##  **Public Methods**
_________________________________________________

### void **DisplayString(string)**

    displays each new line as a new ListElement  
    destroys old ListElements on that List 

### void **DisplayStringList(List<string>)**
    displays each string in a new ListElement   
    destroys old ListElements on that List


### void **DisplayTextAsset(TextAsset)**
    if txt file is attached on Start(), this method will be called
    sets header text to file name
    activates file icon on header
    displays each line of given txt as new row

## void **ClearList()**
    removes all rows

## ShowRowNumbers() / HideRowNumbers 
_// this should be a bool Property_   

    deactivates / activates rowIndexText of every ListElement in rows<>

## TODO: void **Minimize()**
## TODO: void **RevertSize()****
## TODO: void **SetSize()**


________________________________________________
________________________________________________
________________________________________________
________________________________________________
_________________________________________________
________________________________________________
# ListElement (class)
________________________________________________

## **Public Variables**
_________________________________________________

    LayoutElement rowIndexLayoutElement
    MonoBehaviour attachedObject
_________________________________________________
##  **Public Methods**
__________________________________________

### void *SetText(string)*
    sets displayed text to given string

### string *GetTextt()*
    returns displayed text as string

### void  *SetIndexPanelWidth(float)*
    sets width of leftmost panel within    .  used by ListDisplay



_________________________________________________
# Header
    Displays text 
    Displays File icon

    controlled by ListDisplay
_________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________
________________________________________________

                                                     markdown convention below

_________________________________________________
# CLASS NAME
    purpose and principles
_________________________________________________

###    hierarchy dependencies

________________________________________________

## **Public Variables**
_________________________________________________

    var VarName - description
    vat VarName - description
_________________________________________________
##  **Public Methods**
__________________________________________

### type *Method()*
    does what?
### type *Method()*
    does what?

     



         

