﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class draggableTest : MonoBehaviour, IClickable, IDraggable
{
    public Material idleMaterial;
    public Material clickabilityAdvertismentMaterial;

    public Transform draggableTransform;

    public Plane GetPlaneThisIsOn()
    {
        return GetComponentInParent<Plane>();
    }
    


    public Transform GetDraggableTransform()
    {
        return draggableTransform;
    }

    [ExposeMethodInEditor]
    public void OnDragStart()
    {
        Debug.Log("draggabletest drag start");
    }

    [ExposeMethodInEditor]
    public void OnDragEnd()
    {
        Debug.Log("draggabletest drag end");
    }

    [ExposeMethodInEditor]
    public void AdvertiseInteractability()
    {
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.material = clickabilityAdvertismentMaterial;
        }
    }

    [ExposeMethodInEditor]
    public void StopAdvertising()
    {
        Debug.Log("stop advertising");
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.material = idleMaterial;
        }
    }
    [ExposeMethodInEditor]
    public void PrintGlobalPosition()
    {
        Debug.Log(gameObject.name + " globalPosition = " + transform.position );
    }

    public void OnClick()
    {
        Debug.Log("click");
    }

    public Transform OnHoldDetected()
    {
        Debug.Log(gameObject.name + " pointer hold detected");
        return transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(draggableTransform== null)
        {
            Debug.Log(gameObject.name + "transform to drag not set");
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Plane lastPlaneTouched;

    public Plane GetLastPlane()
    {
        return lastPlaneTouched;
    }

    public void RememberPlane(Plane plane)
    {
        lastPlaneTouched = plane;
    }
}
