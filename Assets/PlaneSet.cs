using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
//      A PlaneSet object manages a collection of Plane objects (PlaneSet.planelist)
//      Its purpose is to allow dragging graph elements in the third dimension which is represented by PlaneSet.DraggingDepth property.
//      Currently this is controlled by scrollwheel during dragging.
//
//      Each plane ( has a child mesh that ) can be on one of two layers: permable or impermable
//      Dragging is implemented with raycast. Dragged objects stop on an impermable plane and can be dragged around it.
//      permable planes are ignored while dragging (objects can fall through them further)
//
/// </summary>




public class PlaneSet : MonoBehaviour
{

    public GameObject templatePlane;
    public List<Plane> planes;

    float defaultInterval = 1f;

    int permablePlanesLayer = 8;
    int imPermablePlanesLayer = 9;

    private int draggingDepth;
    public int DraggingDepth
    {
        get { return draggingDepth; }
        set { SetDraggingDepth ( value );}
    }

    [ExposeMethodInEditor]
    public void SetDraggingDepth1()
    {
        DraggingDepth = 1;
    }

    [ExposeMethodInEditor]
    public void SetDraggingDepth2()
    {
        DraggingDepth = 2;
    }


    private void SetDraggingDepth(int newDepth)
    {
        //Debug.Log("dragging depth property setter called: " + newDepth);
        
        if (0 <= newDepth && newDepth < planes.Count)                       // if in range 0-count
        {
            draggingDepth = newDepth;

            for (int index = 0; index < planes.Count; index += 1)
                {
                GameObject rendererGO = planes[index].GetComponentInChildren<MeshRenderer>().gameObject;

                if (index < draggingDepth)                                      
                    {
                    rendererGO.layer = permablePlanesLayer;             // set permable layer
                    //planes[index].Highlight(0);
                    }
                else
                    {
                    rendererGO.layer = imPermablePlanesLayer;           // set impermable layer
                   //planes[index].Highlight(0);

                    if(index == draggingDepth)
                    {
                        //planes[index].Highlight(0);
                    }
                    }
         

                }
            
        }
        //Debug.Log("dragging depth = " + DraggingDepth.ToString());


    }


    public void IncreaseDraggingDepth()
    {
        SetDraggingDepth(DraggingDepth + 1);

    }

    public void DecreaseDraggingDepth()
    {
        SetDraggingDepth(DraggingDepth - 1);
    }

    [ExposeMethodInEditor]
    void AddPlaneAtEnd()
    {
        GameObject newPlaneGO = Instantiate(templatePlane);
        newPlaneGO.SetActive(true);
        newPlaneGO.name = "☐ Plane " + planes.Count;
        newPlaneGO.transform.SetParent(this.transform);
        newPlaneGO.transform.localPosition = new Vector3(0f, 0f, -1* planes.Count * defaultInterval);
        newPlaneGO.transform.localRotation = Quaternion.identity;
        planes.Add(newPlaneGO.GetComponent<Plane>());


    }

    public void SetCount(int newCount)
    {
        for(int c = newCount; c>0; c-=1)
        {
            AddPlaneAtEnd();
        }
    }

    [ExposeMethodInEditor]
    void DestroyLastPlane()
    {
        Destroy(planes[planes.Count - 1].gameObject);
        planes.RemoveAt(planes.Count - 1);
    }



    void Start()
    {
        templatePlane.gameObject.SetActive(false);
        SetCount(20);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
